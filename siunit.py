# ------ Licence and money ------ #
# Author : Simon Zozol, simon.zozol@gmail.com
# This work is under CC0 licence. This is mostly equivalent to public domain.
# The spirit is that you can do whatever you want and I deny any responsibility if it somehow backfire.
# That said, I would appreciate if you would keep the result as open source.
# And I would even more appreciate if you could donate me a little something :
# Tipee : https://www.tipeee.com/holy-python
# Paypal : https://www.paypal.com/us/cgi-bin/webscr?cmd=_send-money&nav=1&email=simon.zozol@gmail.com

import math
from numbers import Real
from collections import namedtuple

u_None = (0,) * 7
u_m = (1, 0, 0, 0, 0, 0, 0)
u_kg = (0, 1, 0, 0, 0, 0, 0)
u_s = (0, 0, 1, 0, 0, 0, 0)
u_A = (0, 0, 0, 1, 0, 0, 0)
u_K = (0, 0, 0, 0, 1, 0, 0)
u_mol = (0, 0, 0, 0, 0, 1, 0)
u_cd = (0, 0, 0, 0, 0, 0, 1)

i_m = 0
i_kg = 1
i_s = 2
i_A = 3
i_K = 4
i_mol = 5
i_cd = 6

units_name = ((u_m, "m", "meter"), (u_kg, "kg", "kilogram"), (u_s, "s", "second"),
              (u_A, "A", "Ampere"), (u_K, "K", "Kelvin"), (u_mol, "mol", "mole"),
              (u_cd, "cd", "candela"))

_dnumber = namedtuple("_dnumber", ["value","unit"], module= "siunit"  )

# D number for Denominate Number. Some number with an abstract unit
class Dnumber(_dnumber, Real):
    # This will create the Dnumber a named tuple
    # Therefore a Dnumber is immutable
    def __new__(cls, value, unit=u_None):
        return _dnumber.__new__(cls, value, unit)

    # No __init__ as this is an immutable

    def _is_unitless(self):
        """return false if number is a Dnumber with a dimension (other than u_None).
           return true otherwise"""
        if isinstance(self, Dnumber):
            if self.unit != u_None:
                return False
        return True

    @property
    def is_unitless(self):
        return self._is_unitless()

    # TODO: see chapter 9.1 (number) about the "clean" addition
    def __add__(self, other):
        """ if both parameters have same units, the result is the sum of values (unit unchanged)
           if units are different, it raise TypeError
           If "other" is not a Dnumber, it works only if self.unit == u_None"""
        try:
            if self.unit != other.unit:
                raise TypeError("Cannot add units " + unit_to_str(self.unit) +
                                " and " + unit_to_str(other.unit))
            return Dnumber(self.value + other.value, self.unit)
        except AttributeError:
            if type(other) in [int, float]:
                return self.add_pure_number(other)
            else:
                return NotImplemented # TODO : additional testing needed

    __radd__ = __add__

    # no __iadd__ for an immutable. `a+=b` will automaticaly execute `a=a+b` with better performances

    def add_pure_number(self, number):
        if self.unit != u_None:
            raise TypeError("Can not add " + type(number).__name__ +
                            " to a number with unit " + unit_to_str(self.unit))
        return Dnumber(self.value + number, self.unit)

    # TODO: performance wise, I certainly can do better
    def __sub__(self, other):
        return self + other * (-1)

    def __mul__(self, other):
        if isinstance(other, Dnumber):
            return Dnumber(self.value * other.value, multiply_units(self.unit, other.unit))
        else:  # we assume other is a unitless number
            return Dnumber(self.value * other, self.unit)

    __rmul__ = __mul__

    def __truediv__(self, other):
        if isinstance(other, Dnumber):
            return Dnumber(self.value / other.value, divide_units(self.unit, other.unit))
        else:  # we assume other is a unitless number
            return Dnumber(self.value / other, self.unit)

    __rtruediv__ = __truediv__

    def __pow__(self, exponent):
        try:
            value = self.value ** exponent
        except TypeError:  # a priori, the conversion below is semantically not wanted
            value = self.value ** float(exponent)
        unit = unit_to_power(self.unit, exponent)
        return Dnumber(value, unit)

    def __rpow__(self, elevated):
        print(self)
        if self._is_unitless():
            return elevated ** self.value
        else:
            raise ValueError("a power has to be unitless")

    def __str__(self):
        return str(self.value) + " " + unit_to_str(self.unit)

    def __neg__(self):
        return self * (-1)

    def __pos__(self):
        return self

    def __abs__(self):
        return Dnumber(abs(self.value), self.unit)

    def __float__(self):
        return float(self.value)

    def __int__(self):
        return int(self.value)

    def __round__(self, ndigit=0):
        return Dnumber(round(self.value, ndigit), self.unit)

    def __ceil__(self):
        return Dnumber(math.ceil(self.value), self.unit)

    def __trunc__(self):
        return Dnumber(math.trunc(self.value), self.unit)

    def __floor__(self):
        return Dnumber(math.floor(self.value), self.unit)

    def __eq__(self, other):
        try:
            if self._is_unitless() and (type(other) in [int, float]):
                return self.value == other

            return (self.value == other.value) and (self.unit == other.unit)
        except:
            return False

    def __floordiv__(self, other):
        return Dnumber.__floor__(self / other)

    def __rfloordiv__(self, other):
        return Dnumber.__floor__(other / self)

    def __le__(self, other):
        if self.unit != other.unit:
            raise TypeError("Cannot compare units " + unit_to_str(self.unit) +
                            " and " + unit_to_str(other.unit))
        return self.value <= other.value

    def __lt__(self, other):
        if self.unit != other.unit:
            raise TypeError("Cannot compare units " + unit_to_str(self.unit) +
                            " and " + unit_to_str(other.unit))
        return self.value < other.value

    def __ge__(self, other):
        if self.unit != other.unit:
            raise TypeError("Cannot compare units " + unit_to_str(self.unit) +
                            " and " + unit_to_str(other.unit))
        return self.value >= other.value

    def __gt__(self, other):
        if self.unit != other.unit:
            raise TypeError("Cannot compare units " + unit_to_str(self.unit) +
                            " and " + unit_to_str(other.unit))
        return self.value > other.value

    # TODO: trouver le vocabulaire pour la division en anglais
    def __mod__(self, divisor):
        if isinstance(divisor, Dnumber):
            divisor = divisor.value
        return Dnumber(self.value % divisor, self.unit)

    def __rmod__(self, divised):
        return divised % self.value


# --- END OF class Dnumber(Real):  ###############


def superscript(number):
    code_superscript= ( '⁰', '¹', '²', '³', '⁴', '⁵', '⁶', '⁷', '⁸', '⁹' )

    try:
        nombre = int(number) # Parameter may be integer or string or whatever. If int(number) fails => valueError
    except ValueError:
        raise ValueError('Value "' + str(number) + '" is not an integer')

    texte2 =''
    if number <0 :
        texte2 = '⁻'
        number = -number
    for lettre in str(number):
        texte2 +=  code_superscript[int(lettre)]

    return texte2

def unit_to_str(unit):
    result = ""
    for i in range(0, 6):
        power = unit[i]
        if power != 0:
            if len(result) > 0:
                result += "."
            if power == 1:
                result += units_name[i][1]
            else:
                result += units_name[i][1] + superscript(power) #   str(power)
    return result


def multiply_units(unit1, unit2):
    return tuple([unit1[i] + unit2[i] for i in range(7)])


def divide_units(unit1, unit2):
    return tuple([unit1[i] - unit2[i] for i in range(7)])


# TODO make a relaxed mode.
def unit_to_power(unit1, exponent):
    if int(exponent) != exponent:
        raise ValueError("a power has to be integer")

    if isinstance(exponent, Dnumber):
        if exponent.is_unitless:
            exponent = float(exponent)
        else:
            raise ValueError("a power has to be unitless")
    return tuple([unit1[i] * exponent for i in range(7)])


def is_unit(arg):
    """ can we interpret the arg as a unit?
    Answer True if it is an enumerate of 7 integers """
    try:
        boolean_tuple = [(int(value) == value) for value in arg]
        return len(boolean_tuple) == 7 and all(boolean_tuple)
    except:
        return False


m = Dnumber(1, u_m)
kg = Dnumber(1, u_kg)
s = Dnumber(1, u_s)
A = Dnumber(1, u_A)
K = Dnumber(1, u_K)
mol = Dnumber(1, u_mol)
cd = Dnumber(1, u_cd)

# Length sub-unit
cm = 1e-2 * m
mm = 1e-3 * m
um = 1e-6 * m
nm = 1e-9 * m
pm = 1e-12 * m
fm = 1e-15 * m
km = 1e3 * m
angstrom = 1e-10 * m
globals()['Å'] = angstrom  # shorter alias (only works in Python 3)
lightyear = 9460730472580800. * m
astro_unit = 149597870700. * m  # astronomical unit
pi = 3.14159265359
pc = (648000. / pi) * astro_unit  # parsec
kpc = 1e3 * pc
Mpc = 1e6 * pc
Gpc = 1e9 * pc
inch = 2.54 * cm
foot = 12. * inch
mile = 5280. * foot
thou = 1e-3 * inch  # thousandth of an inch; also called mil

#  Volume unit and subunit
m3 = m ** 3
L = 1e-3 * m ** 3  # liter. As "dm" is already defined, L=dm**3 also work
mL = 1e-3 * L
uL = 1e-6 * L
nL = 1e-9 * L
pL = 1e-12 * L
fL = 1e-15 * L
aL = 1e-18 * L
kL = 1e3 * L
ML = 1e6 * L
GL = 1e9 * L

# Volume again as liter can be L or l -__-
l = 1e-3 * m ** 3  # liter
ml = 1e-3 * l
ul = 1e-6 * l
nl = 1e-9 * l
pl = 1e-12 * l
fl = 1e-15 * l
al = 1e-18 * l
kl = 1e3 * l
Ml = 1e6 * l
Gl = 1e9 * l

# Time
ms = 1e-3 * s
us = 1e-6 * s
ns = 1e-9 * s
ps = 1e-12 * s
fs = 1e-15 * s
minute = 60. * s
hour = 60. * minute
h = hour
day = 24. * hour  # solar day
week = 7. * day
year = 365.256363004 * day  # sidereal year

# Mass
g = 1e-3 * kg
mg = 1e-3 * g
ug = 1e-6 * g
ng = 1e-9 * g
pg = 1e-12 * g
fg = 1e-15 * g
tonne = 1e3 * kg
amu = 1.660538921e-27 * kg  # atomic mass unit
Da = amu  # Dalton
kDa = 1e3 * Da
lbm = 0.45359237 * kg  # pound mass (international avoirdupois pound)

# Energy
J = (kg * m ** 2) / s ** 2
mJ = 1e-3 * J
uJ = 1e-6 * J
nJ = 1e-9 * J
pJ = 1e-12 * J
fJ = 1e-15 * J
kJ = 1e3 * J
MJ = 1e6 * J
GJ = 1e9 * J
erg = 1e-7 * J
eV = 1.602176565e-19 * J
meV = 1e-3 * eV
keV = 1e3 * eV
MeV = 1e6 * eV
GeV = 1e9 * eV
TeV = 1e12 * eV
btu = 1055.056 * J  # British thermal unit
smallcal = 4.184 * J  # small calorie ("gram calorie")
cal = smallcal
kcal = 4184. * J  # kilocalorie ("large Calorie", "dietary Calorie")
Cal = kcal
Wh = 3600. * J  # w att-hour.  This is a subunit of Joule, not Watt
kWh = 1e3 * Wh  # kilowatt-hour

# Moles, concentration / molarity
atom = 1 / 6.02214129e23 * mol  # Avogadro's number
mmol = 1e-3 * mol
umol = 1e-6 * mol
nmol = 1e-9 * mol
pmol = 1e-12 * mol
fmol = 1e-15 * mol
M = mol / L  # molar
mM = 1e-3 * M
uM = 1e-6 * M
nM = 1e-9 * M
pM = 1e-12 * M
fM = 1e-15 * M

# Forces
N = (kg * m) / s ** 2  # newton
dyn = 1e-5 * N  # dyne
lbf = 4.4482216152605 * N  # pound-force (international avoirdupois pound)

# Pressure
Pa = N / m ** 2  # pascal
hPa = 1e2 * Pa  # hectopascal
kPa = 1e3 * Pa
MPa = 1e6 * Pa
GPa = 1e9 * Pa
bar = 1e5 * Pa
mbar = 1e-3 * bar
cbar = 1e-2 * bar  # centibar
dbar = 0.1 * bar  # decibar
kbar = 1e3 * bar
Mbar = 1e6 * bar
atm = 101325. * Pa
torr = (1. / 760.) * atm
mtorr = 1e-3 * torr
psi = lbf / inch ** 2

# Power
W = J / s
mW = 1e-3 * W
uW = 1e-6 * W
nW = 1e-9 * W
pW = 1e-12 * W
kW = 1e3 * W
MW = 1e6 * W
GW = 1e9 * W
TW = 1e12 * W

# Temperature
degFinterval = (5. / 9.) * K  # A temperature difference in degrees Fahrenheit
degCinterval = K  # A temperature difference in degrees Celsius
mK = 1e-3 * K
uK = 1e-6 * K
nK = 1e-9 * K
pK = 1e-12 * K

# Charge
C = A * s
mC = 1e-3 * C
uC = 1e-6 * C
nC = 1e-9 * C
Ah = 3600. * C  # amp-hour
mAh = 1e-3 * Ah

# Current
# A = C/s      # TODO a verifier. A priori, c'est bien l'ampere qui est le SI et le Coulon qui en decoule
mA = 1e-3 * A
uA = 1e-6 * A
nA = 1e-9 * A
pA = 1e-12 * A
fA = 1e-15 * A

# Voltage
V = J / C
mV = 1e-3 * V
uV = 1e-6 * V
nV = 1e-9 * V
kV = 1e3 * V
MV = 1e6 * V
GV = 1e9 * V
TV = 1e12 * V

# Resistance and conductivity
ohm = V / A
mohm = 1e-3 * ohm
kohm = 1e3 * ohm
Mohm = 1e6 * ohm
Gohm = 1e9 * ohm
S = 1. / ohm  # siemens
mS = 1e-3 * S
uS = 1e-6 * S
nS = 1e-9 * S

# Magnetic fields and fluxes
T = (V * s) / m ** 2  # tesla
mT = 1e-3 * T
uT = 1e-6 * T
nT = 1e-9 * T
G = 1e-4 * T  # gauss
mG = 1e-3 * G
uG = 1e-6 * G
kG = 1e3 * G
Oe = (1000. / (4. * pi)) * A / m  # oersted
Wb = J / A  # weber

# Capacitance and inductance
F = C / V  # farad
uF = 1e-6 * F
nF = 1e-9 * F
pF = 1e-12 * F
fF = 1e-15 * F
aF = 1e-18 * F
H = m ** 2 * kg / C ** 2  # henry
mH = 1e-3 * H
uH = 1e-6 * H
nH = 1e-9 * H

if __name__ == "__main__":
    # let's use this librairy to compute gravitational potential energy of a 5 kg at a 3 m height
    height = 3 * m
    mass = 5 * kg
    g = 9.8 * m / s ** 2
    potential_energy = height * mass * g
    assert (potential_energy == 147 * J)  # that should equal 147 Joules
    # str() function express the energy as m2.kg.s-2. It would be too time consuming to "translate" it as Joule

    assert (is_unit(J.unit))
    assert (is_unit((1, 0, 0, 0, 0, 0, 0)))
    assert (is_unit((1, 0, -2, 0, 0., 0, 0)))
    assert (not is_unit((1, 0, -2.5, 0, 0, 0, 0)))
    assert (not is_unit((1, 0, -2, 0,)))

    assert (Dnumber(2, u_s) == 2 * s)  # Those are 2 ways of writing 2 seconds
    #  check that unit of 2 Joules is, under the hood, a tuple meaning m^2 . kg / s^2
    assert ((2 * J).unit == (2, 1, -2, 0, 0, 0, 0))
